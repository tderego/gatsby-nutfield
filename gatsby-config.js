require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
    siteMetadata: {
      title: "NUTFIELD",
      description: "The spot on the Internet where music and video from Troy DeRego go to rest in peace."
    },
    plugins: [
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          name: `images`,
          path: `${__dirname}/src/images`
        },
      },
        `gatsby-plugin-image`,
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
      {
        resolve: `gatsby-source-drupal`,
        options: {
          baseUrl: `https://nutfield.troyderego.com/`,
          apiBase: `jsonapi`, // optional, defaults to `jsonapi`
        },
      },
        {
        resolve: `gatsby-source-youtube-v3`,
        options: {
          channelId: ['UCwa00H8uwTMyWPcbsjXdB9w'],
          apiKey: process.env.GATSBY_MY_YOUTUBEAPI,
          maxVideos: 50 // Defaults to 50
        },
      },
      {
        resolve: "gatsby-plugin-typography",
        options: {
          pathToConfigModule: "src/utils/typography.js"
        }
      },
    ],
  }
  