import Typography from 'typography'
import twinPeaksTheme from 'typography-theme-twin-peaks'

const typography = new Typography({
  ...twinPeaksTheme,
  headerColor: '#ffffff',
  bodyColor: '#f2f2f2',
  overrideThemeStyles: () => ({
    a: {
      textShadow: null,
    },
  }),
})

export default typography;