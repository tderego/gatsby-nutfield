import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"
import { Card, Col, Container, Row } from "react-bootstrap";
import { GatsbyImage } from "gatsby-plugin-image"

export default function Home({ data }) {
  return (
    <Layout>
      <Container>
        <h1 className="mb-4">NUTFIELD Songs ({data.allNodeSong.totalCount})</h1>
      </Container>
      <Container>
        <Row>
          {data.allNodeSong.edges.map(({ node }) => (
            <Col md={4} className="mb-4">
              <Link to={node.path.alias} className="card-link">
                <Card bg="dark" className="card-hover">
                  <GatsbyImage className="card-img-top" image={node.relationships.field_artwork.localFile.childImageSharp.gatsbyImageData} alt={node.title} />
                  <Card.Body className="p-2">
                    <Card.Title>{node.title}</Card.Title>
                  </Card.Body>
                </Card>
              </Link>
            </Col>
          ))}

        </Row>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    allNodeSong {
      totalCount
      edges {
        node {
          path {
            alias
          }
          title
          field_release_date
          relationships {
            field_artwork {
              localFile {
                publicURL
                childImageSharp {
                  gatsbyImageData(placeholder: BLURRED)
                }
              }
            }
          }
        }
      }
    }
  }
`
