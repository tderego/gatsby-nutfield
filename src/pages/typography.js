import React from "react"
import Layout from "../components/layout"
import { Container } from "react-bootstrap"

export default function Typography() {
    return (
        <Layout>
            <Container>
                <h1>Typography</h1>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
            </Container>
            <Container>
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>
                <h5>Heading 5</h5>
            </Container>
            <Container>
                <p className="display-1">Display 1</p>
                <p className="display-2">Display 2</p>
                <p className="display-3">Display 3</p>
                <p className="display-4">Display 4</p>
                <p className="display-5">Display 5</p>
            </Container>
        </Layout>
    )
}