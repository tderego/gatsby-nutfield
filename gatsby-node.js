const path = require(`path`)
const { slash } = require(`gatsby-core-utils`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  // query content for YouTube vidoes
  const result = await graphql(`
    query {
        allYoutubeVideo {
        edges {
          node {
            id
            videoId
          }
        }
      }
      allNodeSong {
        edges {
          node {
            id
            path {
              alias
            }
          }
        }
      }
    }
  `)

  const postTemplate = path.resolve(`./src/templates/video.js`)
  result.data.allYoutubeVideo.edges.forEach(edge => {
    createPage({
      // `path` will be the url for the page
      path: "/videos/" + edge.node.videoId,
      // specify the component template of your choice
      component: slash(postTemplate),
      context: {
        id: edge.node.id,
      },
    })
  })
  
  const songTemplate = path.resolve(`./src/templates/song.js`)
  result.data.allNodeSong.edges.forEach(edge => {
    createPage({
      // `path` will be the url for the page
      path: edge.node.path.alias,
      // specify the component template of your choice
      component: slash(songTemplate),
      context: {
        id: edge.node.id,
      },
    })
  })
}